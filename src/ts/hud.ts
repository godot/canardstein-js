import * as BABYLON from '@babylonjs/core';
import * as GUI from '@babylonjs/gui';
import Player from './player';

class HUD {

    private readonly _scene: BABYLON.Scene;

    private readonly _ammoPanel: HudField;
    private readonly _healthPanel: HudField;
    private readonly _centeredMessage: GUI.TextBlock;

    private readonly _feedbackPanel: FeedbackPanel;

    constructor(scene: BABYLON.Scene,
                props: {
                    defaultAmmo: number,
                    defaultHealth: number
                }) {
        this._scene = scene;

        const hudTexture: GUI.AdvancedDynamicTexture = GUI.AdvancedDynamicTexture.CreateFullscreenUI("HUD");

        const grid = this.defineGrid();
        hudTexture.addControl(grid);

        this._ammoPanel = new HudField({
            label: "Ammo",
            defaultValue: props.defaultAmmo,
            horizontalAlignment: GUI.Control.HORIZONTAL_ALIGNMENT_LEFT
        });
        grid.addControl(this._ammoPanel, 2, 0);

        this._healthPanel = new HudField({
            label: "Health",
            defaultValue: props.defaultHealth,
            horizontalAlignment: GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT
        });
        grid.addControl(this._healthPanel, 2, 2);

        this._centeredMessage = HudField.createTextBlock({
            text: "You died",
            horizontalAlignment: GUI.Control.HORIZONTAL_ALIGNMENT_CENTER,
            fontSize: 100,
            fontColor: "red",
            height: "100px"
        });
        this._centeredMessage.notRenderable = true;

        grid.addControl(this._centeredMessage, 1, 1);

        this._feedbackPanel = new FeedbackPanel();

        hudTexture.addControl(this._feedbackPanel);
    }

    registerListeners(player: Player) {
        player.setAmmoListener((updatedValue: number) => {
            this._ammoPanel.update(updatedValue);
        });
        player.addHealthListener((updatedValue: number) => {
            this._healthPanel.update(updatedValue);
            if (updatedValue <= 0) {
                this._centeredMessage.notRenderable = false;
            } else {
                this._feedbackPanel.takeDamage(this._scene);
            }
        });
    }

    private defineGrid(): GUI.Grid {
        const grid = new GUI.Grid();
        grid.addColumnDefinition(0.3);
        grid.addColumnDefinition(0.4);
        grid.addColumnDefinition(0.3);
        grid.addRowDefinition(0.3);
        grid.addRowDefinition(0.4);
        grid.addRowDefinition(0.3);

        grid.paddingBottom = "5px";
        grid.paddingLeft = "5px";
        grid.paddingRight = "5px";

        return grid;
    }
}

class HudField extends GUI.StackPanel {

    private readonly _valueTextBlock: GUI.TextBlock;

    constructor(props: {
        label: string,
        defaultValue: number,
        horizontalAlignment: number
    }) {
        super();

        this.verticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_BOTTOM;

        const label = HudField.createTextBlock({
            text: props.label,
            horizontalAlignment: props.horizontalAlignment,
            fontSize: 30,
            fontColor: "white",
            height: "20px"
        });

        this.addControl(label);

        this._valueTextBlock = HudField.createTextBlock({
            text: `${props.defaultValue}`,
            horizontalAlignment: props.horizontalAlignment,
            fontSize: 70,
            fontColor: "white",
            height: "70px"
        });

        this.addControl(this._valueTextBlock);
    }

    update(updatedValue: number) {
        if (updatedValue > 0) {
            this._valueTextBlock.text = `${updatedValue}`;
        } else {
            this._valueTextBlock.text = `0`;
            this._valueTextBlock.color = "red";
        }
    }

    public static createTextBlock(props: {
        text: string,
        horizontalAlignment: number,
        fontSize: number,
        fontColor: string,
        height: string
    }): GUI.TextBlock {
        const textBlock: GUI.TextBlock = new GUI.TextBlock();
        textBlock.text = props.text;
        textBlock.fontSize = props.fontSize;
        textBlock.color = props.fontColor;
        textBlock.fontFamily = "Wolfenstein";
        textBlock.height = props.height;
        textBlock.textHorizontalAlignment = props.horizontalAlignment;
        textBlock.shadowOffsetX = 2;
        textBlock.shadowOffsetY = 2;

        return textBlock;
    }
}

class FeedbackPanel extends GUI.Rectangle {

    private readonly animations = [];

    constructor() {
        super();
        this.width = "100%";
        this.height = "100%";
        this.background = "red";
        this.alpha = 0;

        this.animations.push(this.createDamageAnimation());
    }

    private createDamageAnimation(): BABYLON.Animation {
        const damageAnimation = new BABYLON.Animation("damageAnimation", "alpha", 30, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
        damageAnimation.setKeys([
            {frame: 0, value: 0},
            {frame: 2, value: 0.6},
            {frame: 10, value: 0.0}
        ]);

        return damageAnimation;
    }

    public takeDamage(scene: BABYLON.Scene) {
        scene.beginAnimation(this, 0, 10, false);
    }
}

export default HUD;