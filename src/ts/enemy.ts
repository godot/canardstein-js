import * as BABYLON from '@babylonjs/core';
import Player from './player';

class Enemy {

    private readonly _sprite: BABYLON.Sprite;
    private readonly _speed: number;
    private readonly _spriteRadius: number;

    private readonly _weaponSound: BABYLON.Sound;
    private readonly _weaponDamage: number;
    private readonly _weaponRange: number;

    private readonly _shootFrameInterval: number;
    private readonly _shootInterval: number;

    private _currentFrameInterval: number;
    private _currentShootInterval: number;

    private _state: EnemyState;
    private _health: number;

    constructor(sprite: BABYLON.Sprite, position: BABYLON.Vector3, weaponSound: BABYLON.Sound) {
        this._sprite = sprite;
        this._sprite.position = position;
        this._speed = 0.0125;
        this._spriteRadius = 0.25;

        this._weaponSound = weaponSound;
        this._weaponRange = 3;
        this._weaponDamage = 5;

        this._shootFrameInterval = 250;
        this._shootInterval = 1500;

        this._currentFrameInterval = this._shootFrameInterval;
        this._currentShootInterval = 250;

        this._state = EnemyState.STANDING;
        this._health = 10;
    }

    startMoving(playerPosition: BABYLON.Vector3, walls: boolean[][]) {
        if (!this.isMoving()) {
            this._state = EnemyState.MOVING;
            this._sprite.playAnimation(0, 1, true, 200, () => {
            });
        }

        this.move(playerPosition, walls);
    }

    move(playerPosition: BABYLON.Vector3, walls: boolean[][]) {
        const direction = playerPosition.subtract(this._sprite.position).multiplyByFloats(this._speed, 0, this._speed);
        if (!this.tryMoving(direction, walls)) {
            const slideToXDirection = new BABYLON.Vector3(direction.x, 0, 0);
            if (!this.tryMoving(slideToXDirection, walls)) {
                const slideToZDirection = new BABYLON.Vector3(0, 0, direction.z);
                this.tryMoving(slideToZDirection, walls);
            }
        }
    }

    canHit(playerPosition: BABYLON.Vector3, scene: BABYLON.Scene): boolean {
        const origin = this._sprite.position;
        const playerForward = playerPosition.subtract(origin);
        const direction = BABYLON.Vector3.Normalize(playerForward);
        const ray = new BABYLON.Ray(origin, direction, this._weaponRange);

        const result = scene.pickWithRay(ray) as BABYLON.PickingInfo;
        if (result.hit) {
            const pickedMesh = result.pickedMesh as BABYLON.Mesh;
            const meshForward = pickedMesh.position.subtract(origin);
            return meshForward.length() > playerForward.length();
        } else {
            return playerForward.length() <= this._weaponRange;
        }
    }

    startShooting(player: Player, deltaTime: number) {
        if (this.isMoving()) {
            this._sprite.stopAnimation();
        }
        this._state = EnemyState.SHOOTING;
        this.animateShooting(deltaTime);
        this.shoot(player);
    }

    private animateShooting(deltaTime: number) {
        this._currentShootInterval -= deltaTime;
        this._currentFrameInterval -= deltaTime;
        if (this._currentShootInterval <= 0) {
            if (this._currentFrameInterval <= 0) {
                this._sprite.cellIndex = 2;
                this._currentFrameInterval = this._shootFrameInterval;
                this._currentShootInterval = this._shootInterval;
            }
        } else {
            if (this._currentFrameInterval <= 0) {
                this._sprite.cellIndex = 0;
                this._currentFrameInterval = this._shootFrameInterval;
            }
        }
    }

    private shoot(player: Player) {
        if (this.isShooting()) {
            this._weaponSound.play();
            player.takeDamage(this._weaponDamage)
        }
    }

    private isShooting(): boolean {
        return this._currentShootInterval === this._shootInterval;
    }

    private tryMoving(direction: BABYLON.Vector3, walls: boolean[][]): boolean {
        const newPosition = this._sprite.position.add(direction);

        const minX: number = Math.round(newPosition.x - this._spriteRadius);
        const maxX: number = Math.round(newPosition.x + this._spriteRadius);

        const minY: number = Math.round(newPosition.z - this._spriteRadius);
        const maxY: number = Math.round(newPosition.z + this._spriteRadius);

        const mapSize = walls.length;
        for (let x = minX; x <= maxX; x++) {
            for (let y = minY; y <= maxY; y++) {
                if ((x < 0) || (y < 0) || (x >= mapSize) || (y >= mapSize)) {
                    return false;
                }
                if (walls[x][y]) {
                    return false;
                }
            }
        }
        this._sprite.position = newPosition;
        return true;
    }

    isMoving(): boolean {
        return this._state === EnemyState.MOVING;
    }

    isAlive(): boolean {
        return this._state !== EnemyState.DEAD;
    }

    takeDamage(damagePoints: number) {
        this._health -= damagePoints;
        if (this._health <= 0) {
            this._state = EnemyState.DEAD;
            this._sprite.playAnimation(3, 5, false, 500, () => {
            });
        }
    }

    getSprite(): BABYLON.Sprite {
        return this._sprite;
    }
}

enum EnemyState {
    STANDING, MOVING, SHOOTING, DEAD
}

export default Enemy;