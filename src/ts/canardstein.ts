// Copyright 2018 David Boissier. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.

import * as BABYLON from '@babylonjs/core';
import Player from './player';
import Enemy from './enemy';
import HUD from './hud';

class Game {
    private readonly _canvas: HTMLCanvasElement;
    private readonly _engine: BABYLON.Engine;
    private _scene: BABYLON.Scene;
    private _camera: BABYLON.UniversalCamera;
    private _enemySpriteManager: BABYLON.SpriteManager;
    private _wallFactory: WallFactory;
    private _player: Player;

    private _hud: HUD;

    private readonly _enableCollisions: boolean;
    private readonly _mapSize: number;
    private readonly _walls: boolean[][];

    private readonly _enemyPositions: Array<BABYLON.Vector3>;
    private readonly _enemies: Array<Enemy>;

    constructor(canvasElementId: string) {
        this._canvas = document.getElementById(canvasElementId) as HTMLCanvasElement;
        this._engine = new BABYLON.Engine(this._canvas, true);

        this._enableCollisions = true;
        this._mapSize = 32;
        this._enemyPositions = [
            new BABYLON.Vector3(6, -0.045, 26)
        ];
        this._enemies = [];
        this._walls = Game.initDefaultMapWalls(this._mapSize);
    }

    createScene(): void {
        this._scene = new BABYLON.Scene(this._engine);
        this._scene.clearColor = new BABYLON.Color4(0, 0, 255, 1);
        this._scene.gravity = new BABYLON.Vector3(0, -9.81, 0);
        this._scene.collisionsEnabled = this._enableCollisions;

        this._enemySpriteManager = new BABYLON.SpriteManager("enemySpriteManager", "images/nazi.png",
            this._enemyPositions.length, {
                width: 256,
                height: 256
            },
            this._scene);
        this._enemySpriteManager.isPickable = true;

        this._wallFactory = new WallFactory(this._scene, this._enableCollisions);

        this.createCamera();

        this.createGunSightWithOrthographicCamera();

        this.createHUD();

        this.createAmbientLight();

        this.createLevel();

        this.createGround();

        this.createCeiling();

        this.createEnemies();

        this.registerGameListeners();
    }

    private createHUD() {
        this._hud = new HUD(
            this._scene,
            {
                defaultAmmo: this._player.getAmmo(),
                defaultHealth: this._player.getHealth(),
            });
    }

    private createCamera() {
        this._camera = new BABYLON.UniversalCamera('MainCamera', new BABYLON.Vector3(4, 0, 20), this._scene);
        this._camera.setTarget(new BABYLON.Vector3(4, 0, 30));
        this._camera.speed = 0.1; //set walk speed
        this._camera.minZ = 0.01; // set clipping distance
        this._camera.angularSensibility = 1000; //mouse sensibility
        this._camera.keysUp = [90]; // Z Key
        this._camera.keysDown = [83]; // S Key
        this._camera.keysLeft = [81]; // Q Key
        this._camera.keysRight = [68]; // D Key
        this._camera.applyGravity = true;
        this._camera.checkCollisions = this._enableCollisions;
        this._camera.ellipsoid = new BABYLON.Vector3(0.15, 0.25, 0.15);

        this._camera.attachControl(this._canvas, true);

        this._scene.activeCameras.push(this._scene.activeCamera);
    }

    private createGunSightWithOrthographicCamera() {
        const secondCamera = new BABYLON.FreeCamera("GunSightCamera", new BABYLON.Vector3(0, 0, -1), this._scene);
        secondCamera.mode = BABYLON.Camera.ORTHOGRAPHIC_CAMERA;
        secondCamera.layerMask = 0x10000000;
        this._scene.activeCameras.push(secondCamera);

        const imageHeight = 512;
        const planeSize = imageHeight * 0.75;
        const gunSight = BABYLON.MeshBuilder.CreatePlane("gunsight", {
            width: planeSize,
            height: planeSize
        }, this._scene);
        gunSight.layerMask = secondCamera.layerMask;
        const gunSightY = (this._canvas.clientHeight - planeSize) / 2;
        gunSight.position = new BABYLON.Vector3(0, -gunSightY, 0);

        const gunMaterial = new BABYLON.StandardMaterial("gunMaterial", this._scene);
        gunMaterial.emissiveColor = BABYLON.Color3.White();
        gunMaterial.disableLighting = true;

        const gunTexture = new BABYLON.Texture("images/pistolet.png", this._scene);
        gunTexture.hasAlpha = true;
        gunTexture.uScale = 1 / 3;
        gunTexture.vScale = 1;

        gunMaterial.diffuseTexture = gunTexture;
        gunSight.material = gunMaterial;

        gunSight.freezeWorldMatrix();

        this._player = new Player(gunSight, this._scene);
    }

    private createAmbientLight() {
        const light: BABYLON.Light = new BABYLON.HemisphericLight("HemisphericLight", new BABYLON.Vector3(0, 1, 0), this._scene);
        light.intensity = 0;
        light.excludeWithLayerMask = 0x10000000;
    }

    private createLevel() {
        const levelMap = new BABYLON.Texture("images/carte.png", this._scene,
            false, true, BABYLON.Texture.TRILINEAR_SAMPLINGMODE,
            async () => {
                const mapPixels = await levelMap.readPixels();

                let x = 0;
                let y = 0;

                for (let index = 0; index < mapPixels.byteLength - 4; index += 4) {
                    const color: BABYLON.Color3 = new BABYLON.Color3(mapPixels[index], mapPixels[index + 1], mapPixels[index + 2]);

                    const isCreated = this._wallFactory.create(color, new BABYLON.Vector3(x, 0, y));
                    if (isCreated) {
                        this._walls[x][y] = isCreated;
                    }

                    x++;
                    if (x % this._mapSize == 0) { //end of line of pixels reached
                        x = 0;
                        y++;
                    }
                }
            }
        );
    }

    private createGround() {
        const ground: BABYLON.Mesh = BABYLON.MeshBuilder.CreatePlane("ground", {
            width: this._mapSize,
            height: this._mapSize
        }, this._scene);

        ground.rotation.x = Math.PI / 2;
        ground.position = new BABYLON.Vector3((this._mapSize - 1) / 2, -0.5, (this._mapSize - 1) / 2);
        ground.checkCollisions = this._enableCollisions;

        const groundMaterial: BABYLON.StandardMaterial = new BABYLON.StandardMaterial("gMaterial", this._scene);
        const groundTexture: BABYLON.Texture = new BABYLON.Texture("./images/sol.png", this._scene);
        groundTexture.uScale = this._mapSize;
        groundTexture.vScale = this._mapSize;
        groundTexture.uOffset = 0.375;

        groundMaterial.lightmapTexture = groundTexture;

        ground.material = groundMaterial;
    }

    private createCeiling() {
        const ceiling: BABYLON.Mesh = BABYLON.MeshBuilder.CreatePlane("ceiling", {
            width: this._mapSize,
            height: this._mapSize
        }, this._scene);

        ceiling.rotation.x = BABYLON.Tools.ToRadians(-90);
        ceiling.position = new BABYLON.Vector3((this._mapSize - 1) / 2, 0.5, (this._mapSize - 1) / 2);

        const ceilingMaterial: BABYLON.StandardMaterial = new BABYLON.StandardMaterial("gMaterial", this._scene);
        const ceilingTexture: BABYLON.Texture = new BABYLON.Texture("./images/plafond.png", this._scene);
        ceilingTexture.uScale = this._mapSize;
        ceilingTexture.vScale = this._mapSize;
        ceilingTexture.uOffset = 0.375;

        ceilingMaterial.lightmapTexture = ceilingTexture;

        ceiling.material = ceilingMaterial;
    }

    private createEnemies() {
        const shootSound = new BABYLON.Sound("enemyGunShot", "sounds/pistolet.wav", this._scene);
        this._enemyPositions.forEach(position => {
            const sprite = new BABYLON.Sprite("enemySprite", this._enemySpriteManager);
            sprite.isPickable = true;
            this._enemies.push(new Enemy(sprite, position, shootSound));
        })
    }

    private updateEnemiesAction() {
        if (this._player.isDead()) {
            return;
        }
        this._enemies
            .filter(enemy => enemy.isAlive())
            .forEach(enemy => {
                if (enemy.canHit(this._camera.position, this._scene)) {
                    const deltaTime = this._scene.getEngine().getDeltaTime();
                    enemy.startShooting(this._player, deltaTime);
                } else {
                    enemy.startMoving(this._camera.position, this._walls)
                }
            });
    }

    static initDefaultMapWalls(size: number): Array<Array<boolean>> {
        let matrix = [];
        for (let x = 0; x < size; x++) {
            let row = [];
            for (let y = 0; y < size; y++) {
                row.push(false);
            }
            matrix.push(row);
        }
        return matrix;
    }

    doRender(): void {
        this._engine.runRenderLoop(() => {
            this.updateEnemiesAction();

            this._scene.render();
        });

        window.addEventListener('resize', () => {
            this._engine.resize();
        });

        window.addEventListener('click', (evt: MouseEvent) => {
            this._player.shoot(this._enemies, this._camera);
        });
    }

    private registerGameListeners() {
        this._hud.registerListeners(this._player);

        this._player.addHealthListener((updatedValue: number) => {
            if (updatedValue <= 0) {
                this._camera.inputs.clear();
            }
        });
    }
}

class WallFactory {

    private readonly _scene: BABYLON.Scene;
    private readonly _enableCollisions: boolean;
    private readonly _normalWallTexture: BABYLON.Texture;
    private readonly _decoratedWallTexture: BABYLON.Texture;

    private _normalWall: BABYLON.Mesh;
    private _decoratedWall: BABYLON.Mesh;

    constructor(scene: BABYLON.Scene, enableCollisions: boolean) {
        this._scene = scene;
        this._enableCollisions = enableCollisions;
        this._normalWallTexture = new BABYLON.Texture("images/mur_mur90.png", this._scene);
        this._decoratedWallTexture = new BABYLON.Texture("images/mur_deco_mur_deco90.png", this._scene);
    }

    create(color: BABYLON.Color3, position: BABYLON.Vector3): boolean {
        if (this.isNormalWall(color)) {
            this.createNormalWall(position);
            return true;
        } else if (this.isDecoratedWall(color)) {
            this.createDecoratedWall(position);
            return true;
        }
        return false;
    }

    private createNormalWall(position: BABYLON.Vector3): void {
        if (this._normalWall != null) {
            this.cloneWall(position, this._normalWall);
        } else {
            this._normalWall = this.createWall(position, this._normalWallTexture);
        }
    }

    private createDecoratedWall(position: BABYLON.Vector3): void {
        if (this._decoratedWall != null) {
            this.cloneWall(position, this._decoratedWall);
        } else {
            this._decoratedWall = this.createWall(position, this._decoratedWallTexture);
        }
    }

    private cloneWall(position: BABYLON.Vector3, wall): void {
        const clonedWall: BABYLON.InstancedMesh = wall.createInstance(`MyWall_${position.x}_${position.z}`);
        clonedWall.position = position;
        clonedWall.checkCollisions = this._enableCollisions;
    }

    private createWall(position: BABYLON.Vector3, wallTexture: BABYLON.Texture): BABYLON.Mesh {
        const faceUV = new Array(4);
        faceUV[0] = new BABYLON.Vector4(0.5, 1, 0, 0);//back
        faceUV[1] = new BABYLON.Vector4(0, 0, 0.5, 1);//front
        faceUV[2] = new BABYLON.Vector4(0.5, 0, 1, 1);//left
        faceUV[3] = new BABYLON.Vector4(0.5, 0, 1, 1);//right

        const options = {
            size: 1,
            faceUV: faceUV
        };

        const wall: BABYLON.Mesh = BABYLON.MeshBuilder.CreateBox(`MyWall_${position.x}_${position.z}`, options);

        wall.position = position;
        wall.checkCollisions = this._enableCollisions;

        const wallMaterial: BABYLON.StandardMaterial = new BABYLON.StandardMaterial("MyWallMaterial", this._scene);
        wallMaterial.lightmapTexture = wallTexture;

        wall.material = wallMaterial;

        return wall;
    }

    private isNormalWall(color: BABYLON.Color3): boolean {
        return (color.r == 255) && (color.g == 255) && (color.b == 255);
    }

    private isDecoratedWall(color: BABYLON.Color3): boolean {
        return (color.r == 0) && (color.g == 0) && (color.b == 255);
    }
}

window.addEventListener('DOMContentLoaded', () => {
    // Create the game using the 'renderCanvas'.
    const game = new Game('renderCanvas');

    // Create the scene.
    game.createScene();

    // Start render loop.
    game.doRender();
});